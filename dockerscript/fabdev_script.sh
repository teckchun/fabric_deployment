#! /bin/bash
#
# Copyright IBM Corp All Rights Reserved
#
# SPDX-License-Identifier: Apache-2.0
#

# This script will orchestrate a sample end-to-end execution of the Hyperledger
# Fabric network.
#
# The end-to-end verification provisions a sample Fabric network consisting of
# two organizations, each maintaining two peers, and a “solo” ordering service.
#
# This verification makes use of two fundamental tools, which are necessary to
# create a functioning transactional network with digital signature validation
# and access control:
#
# * cryptogen - generates the x509 certificates used to identify and
#   authenticate the various components in the network.
# * configtxgen - generates the requisite configuration artifacts for orderer
#   bootstrap and channel creation.
#
# Each tool consumes a configuration yaml file, within which we specify the topology
# of our network (cryptogen) and the location of our certificates for various
# configuration operations (configtxgen).  Once the tools have been successfully run,
# we are able to launch our network.  More detail on the tools and the structure of
# the network will be provided later in this document.  For now, let's get going...

# prepending $PWD/../bin to PATH to ensure we are picking up the correct binaries
# this may be commented out to resolve installed version of tools if desired
export PATH=${PWD}/../bin:${PWD}:$PATH
export FABRIC_CFG_PATH=${PWD}
export VERBOSE=false
export COMPOSE_HTTP_TIMEOUT=200
export COMPOSE_PROJECT_NAME='fabdep'
# channel name 
OPERATION_TYPE=$1
NETWORK_NAME=$2
CLI_NAME=$NETWORK_NAME'cli'

function createNetwork(){
  # now run the end to end script
  if [ "$OPERATION_TYPE" = "install" ]
  then
    docker-compose -f docker-compose-cli.yaml up -d
    sleep 60
   # now run the end to end script
    # path to the fabdev script
    docker exec "$CLI_NAME" scripts/network.sh
   else
    docker exec "$CLI_NAME" scripts/upgrade.sh
  fi
  
}

sleep 5

createNetwork 


