# this func install NVM and nodejs
installNodeAndNvm(){
    echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    echo @@@@@@@@@@@@@@@@@@@@@@ NVM INSTALLATION @@@@@@@@@@@@@@@@@@@@@@
    echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    sudo apt-get update
    sudo apt-get -y install build-essential libssl-dev

    if ([ -f ./install_nvm.sh ]) then
        echo
        echo nvm shell file already present
        echo
    else
        echo
        echo downloading the shell file
        echo
        curl -sL https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh -o install_nvm.sh
    fi

    sh install_nvm.sh

    sleep 1

    if ([ -f $HOME/.profile ]) then
        . ~/.profile
    fi

    sleep 1

    export NVM_DIR="$HOME/.nvm"
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
    [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" # This loads nvm bash_completion

    echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    echo @@@@@@@@@@@@@@@@ Installing Nodejs 8.11.3 LTS @@@@@@@@@@@@@@@@@
    echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    nvm install 8.11.3
    nvm alias default 8.11.3
    nvm use default
    if ([ -f $HOME/.profile ]) then
        . $HOME/.profile
    fi
}

# Determine whether execute go build in foreground or background
askProceed(){
    read -p "Press f to run fabdep in foreground and press b to run in background [f/b]  " ARG
    case "$ARG" in
    f | F)
        echo
        echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ 
        echo @@@@@@@@ fabdep started in foreground @@@@@@@@@@ 
        echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ 
        echo
        ./fabdep >/dev/null 2>log.txt &
        sleep 4
        xdg-open http://localhost/fabdep-frontend/index.html
        pkill fabdep
        rm -rf log.txt
        echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        echo @@@@@@@@ fabdep frontend running on http://localhost/fabdep-frontend/index.html @@@@@@@@
        echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        ./fabdep
        ;;
    b | B | "")
        echo
        echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ 
        echo @@@@@@@@ fabdep started in background @@@@@@@@@@ 
        echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ 
        echo
        ./fabdep >/dev/null 2>log.txt &
        xdg-open http://localhost/fabdep-frontend/index.html 
        echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        echo @@@@@@@@ fabdep frontend running on http://localhost/fabdep-frontend/index.html @@@@@@@@
        echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        ;;
    *)
        echo "invalid response"
        askProceed
        ;;
    esac
    
}

# ask user to override hyperledger fabric binaries
askOverrideBinaries(){
    read -p "Press Y to continue override fabric binaries and N to setup manually fabric binaries version 1.2 [Y/N]  " ARG
    case "$ARG" in
    y | Y)
        if ([ -f hyperledger-fabric-linux-amd64-1.2.0.tar.gz ]) then
            echo
            echo
        else
            echo
            echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            echo @@@@@@@@ Downloading hyperledger fabric binaries version 1.2  @@@@@@@@@@
            echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            echo
            
            wget https://nexus.hyperledger.org/content/repositories/releases/org/hyperledger/fabric/hyperledger-fabric/linux-amd64-1.2.0/hyperledger-fabric-linux-amd64-1.2.0.tar.gz    
        fi

        EXIT_STATUS=$?
        
        if [ "$EXIT_STATUS" -eq "0" ]
        then
            tar -xvzf hyperledger-fabric-linux-amd64-1.2.0.tar.gz > /dev/null
            cd bin
            addpresentworkingdir
            cd ..
        else
            echo
            echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            echo @@@@@@@@@@ Unable to download hyperledger fabric binaries @@@@@@@@@@@@@@@@@
            echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            echo
        fi
        ;;
    n | N)
        echo 
        echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        echo @@@@@@@@ Please setup hyperledger fabric binaries version 1.2 manually @@@@@@@@ 
        echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        echo 
        exit 0
        ;;
    *)
        echo "invalid response"
        askOverrideBinaries
        ;;
    esac
}

# add present working directory to ~/.profile file
addpresentworkingdir(){
    # Add Present Directory Path in Profile Envirement Variable File
    if (cat $HOME/.profile | grep "$PWD" > /dev/null 2>&1) then
        echo
    else
        sed -i '$a export PATH='$PWD:'$PATH' $HOME/.profile
    fi

    if ([ -f $HOME/.profile ]) then
        . $HOME/.profile
    fi
}

# check command already exist or not
command_exists() {
	command -v "$@" > /dev/null 2>&1
}

# check /home/.fabdep dir already exits or not
if [ ! -d $HOME/.fabdep ]; then
    # make fabdep dir
    mkdir -p $HOME/.fabdep
fi

if [ $PWD != $HOME/.fabdep ]; then
    # Copy fabdep to /home/fabdep
    yes | cp -rf ./* $HOME/.fabdep
    cd $HOME/.fabdep
fi

# check ubuntu version is supported by fabdep
if (lsb_release -ds | grep "Ubuntu 14" || lsb_release -ds | grep "Ubuntu 15" || lsb_release -ds | grep "Ubuntu 16") then
    echo  
else
    echo 
    echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    echo @@@@@@@ Your operating system not supported by FabDep @@@@@@@@@@@@@@
    echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    echo 
    exit 1
fi

# check nodejs installed
if (which node) then
    echo 
    echo node.js already exists
    echo
else
    installNodeAndNvm
fi

if (command_exists curl) then
    echo 
    echo curl already exists
    echo
else
    sudo apt-get -y install curl
fi

if ( command_exists ng && ng --version | grep 'Angular CLI: 6' ) then
    echo
    echo angular already exists
    echo
else
    nodepath=$(which node)
    node_dir="/usr/bin/node"

    # check nodejs is installed by sudo
    if [ $nodepath = $node_dir ]
    then
        sudo npm install -g @angular/cli@6.0.2
    else
        npm install -g @angular/cli@6.0.2
    fi
fi


# check apache2 installed
if (command_exists apache2) then
    echo
    echo apache2 already exists
    echo
else
    echo 
    echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    echo @@@@@@@@@@@@ downloading apache2 @@@@@@@@@@@@@@@@
    echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    echo 
    sudo apt install apache2
    sleep 4
    sudo service apache2 start
fi

# check sqlite db installed
if (command_exists sqlite3) then
    echo
    echo sqlite3 already exists
    echo
else
    echo 
    echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    echo @@@@@@@@@@@@ downloading sqlite3 @@@@@@@@@@@@@@@@
    echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    echo
    sudo apt-get update
    sudo apt-get install sqlite3 libsqlite3-dev
    sleep 2
fi

# check python installed
if (command_exists python2.7) then
    echo
    echo Python 2.7 already exists
    echo
else
    echo 
    echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    echo @@@@@@@@@@@@ downloading Python 2.7 @@@@@@@@@@@@@@@
    echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    echo 
    sudo add-apt-repository ppa:fkrull/deadsnakes
    sudo apt-get update
    sleep 4
    sudo apt-get install python2.7
fi

# check ansible installed
if (command_exists ansible) then
    echo 
    echo Ansible already exists
    echo
else
    echo 
    echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    echo @@@@@@@@@@@@@@@@ Installing Ansible @@@@@@@@@@@@@@@@@@
    echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    echo 
    sudo apt-get update
    sudo apt-get install software-properties-common
    sudo apt-add-repository --yes --update ppa:ansible/ansible-2.7
    sudo apt-get install ansible
    sleep 4
fi

addpresentworkingdir

# check docker installed
if (command_exists docker) then
    echo 
    echo Docker already exists
    echo
else
    echo
    echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    echo @@@@@@@@@@@@@@@@ Installing Docker @@@@@@@@@@@@@@@@@@@@@@@
    echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    echo
    sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo apt-key fingerprint 0EBFCD88
    sudo add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable"
    sudo apt-get update
    sudo apt-get install -y docker-ce
    # Linux post-install
    sudo groupadd docker ; \
    sudo usermod -aG docker ${USER} && \
    sudo systemctl enable docker
    sudo chown $USER:docker /var/run/docker.sock
    sleep 2
fi

# check docker-compose installed
if (command_exists docker-compose) then
    echo 
    echo Docker Compose already exists
    echo
else
    echo 
    echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    echo @@@@@@@@@@@@@@@@ Installing Docker Compose @@@@@@@@@@@@@@@@@@@
    echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    echo 
    sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose && \
    sudo chmod +x /usr/local/bin/docker-compose
    sleep 2
fi

export FABRIC_TAG=1.2.1
export THIRDPARTY_TAG=0.4.14
export CA_TAG=1.2.1

dockerFabricPull() {
  for IMAGES in peer orderer ccenv javaenv tools; do
    echo
    echo "==> FABRIC IMAGE: $IMAGES"
    echo
    docker pull hyperledger/fabric-$IMAGES:$FABRIC_TAG
    docker tag hyperledger/fabric-$IMAGES:$FABRIC_TAG hyperledger/fabric-$IMAGES
  done
}

dockerCaPull() {
    echo
    echo "==> FABRIC CA IMAGE"
    echo
    docker pull hyperledger/fabric-ca:$CA_TAG
    docker tag hyperledger/fabric-ca:$CA_TAG hyperledger/fabric-ca
}

dockerThirdPartyImagesPull() {
  for IMAGES in couchdb kafka zookeeper; do
    echo
    echo "==> THIRDPARTY DOCKER IMAGE: $IMAGES"
    echo
    docker pull hyperledger/fabric-$IMAGES:$THIRDPARTY_TAG
    docker tag hyperledger/fabric-$IMAGES:$THIRDPARTY_TAG hyperledger/fabric-$IMAGES
  done
}

dockerInstall() {
    if (command_exists docker) then
    
        sudo groupadd docker ; \
        sudo usermod -aG docker ${USER} && \
        sudo chown $USER:docker /var/run/docker.sock

        echo  
        echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        echo @@@@@@@@ Installing Hyperledger Fabric docker images for version 1.2.1 @@@@@@@@@@
        echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        echo

        echo
        echo "===> Pulling fabric Images"
        echo
        dockerFabricPull
        echo
        echo "===> Pulling fabric ca Image"
        echo
        dockerCaPull
        echo
        echo "===> Pulling thirdparty docker images"
        echo
        dockerThirdPartyImagesPull 
    else
        dockerInstall
    fi
}

# check crypogen binaries already exists
if (command_exists cryptogen) then
    # check crypogen version is supported by fabdep
    if (cryptogen version | grep "Version: 1.2") then
        echo 
        echo Cryptogen 1.2 already exists
        echo
    else
        askOverrideBinaries
    fi
else
    # check tar file of all fabric binaries is exists
    if [ -f hyperledger-fabric-linux-amd64-1.2.0.tar.gz ] 
    then
        echo
        echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        echo @@@@@@@@ hyperledger-fabric-linux-amd64-1.2.0.tar.gz already exists @@@@@@@@
        echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        echo
    else
        # download hyperledger fabric binaries 1.2 version for nexus.hyperledger.org
        wget https://nexus.hyperledger.org/content/repositories/releases/org/hyperledger/fabric/hyperledger-fabric/linux-amd64-1.2.0/hyperledger-fabric-linux-amd64-1.2.0.tar.gz    
    fi
    
    EXIT_STATUS=$?
    if [ "$EXIT_STATUS" -eq "0" ]
    then
        rm -rf bin
        rm -rf config
        # extract tar file
        tar -xvzf hyperledger-fabric-linux-amd64-1.2.0.tar.gz > /dev/null
        cd bin
        # add bin path to ~/.profile evn file
        addpresentworkingdir
        cd ..
    else
        echo
        echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        echo @@@@@@@@ Unable to download hyperledger fabric binaries @@@@@@@@@@@@@@
        echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        echo
    fi
fi

dockerInstall

# check html dir already exits or not
if [ ! -d "/var/www/html" ]; then
    # make html dir
    sudo mkdir -p /var/www/html
    sudo chmod -R 777 /var/www/html
fi

# remove previous fabdep-frontend dir if exists
sudo rm -rf /var/www/html/fabdep-frontend

sudo mkdir -p /var/www/html/fabdep-frontend

# copy fabdep-frontend dir to /var/www/html dir
sudo cp -R fabdep-frontend/* /var/www/html/fabdep-frontend

sleep 2

sudo chmod -R 777 /var/www/html/fabdep-frontend

# kill fabdep proccess if already running
pkill fabdep

echo "[Desktop Entry]
Encoding=UTF-8
Name=Fabdep
Exec=/bin/bash -c 'cd $HOME/.fabdep && ./fabdep'
Icon=$HOME/.fabdep/fabdep.png
Terminal=true
Type=Application
Categories=Development;
Name[en_IN]=Fabdep" > ./Fabdep.desktop
cp -f ./Fabdep.desktop $HOME/Desktop/Fabdep.desktop && chmod u+x $HOME/Desktop/Fabdep.desktop
cp -f ./Fabdep.desktop $HOME/.local/share/applications/Fabdep.desktop

askProceed

