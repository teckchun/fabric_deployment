## \*\*FabDep User Manual\*\*

1. Before proceeding with the installation, you need to make sure the necessary software requirements are met. For that, you need to open the FabDep folder in the terminal and run the "fabdep.sh" Shell script using the command: `sh ./fabdep.sh`, which will automatically set up all the prerequisites mentioned below.

   ### For system:

    - Ansible
    
    - Node.js

    - Apache2 & above web server

    - Sqlite3

    - Angular JS 

    - Docker CE

    - Docker Compose

    - Hyperledger fabric binaries version 1.2

    - Hyperledger Fabric docker images for version 1.2.1

    - Linux integrated Ubuntu-based 64-bit Operating system

    - Python 2.7 or above

   ### For server to be used in the LAN/Cloud network:

    - You have the option of deploying the network either on LAN or cloud 

    - If you're using Kafka consensus, then your system should have minimum 2GB RAM in order to deploy Kafka and Zookeeper

    - Good internet speed

    - Ubuntu 16.04.5 LTS

    - 2 GB RAM

    - OpenSSH server should be installed

    - User Name and Password should be configured for the user

    - Python 2.7 or above

   ### For system to be used in the docker network:

    - You have the option of deploying the network on your system with docker

    - If you're using Kafka consensus, then your system should have minimum 8GB RAM in order to deploy Kafka and Zookeeper

    - Good internet speed

    - Ubuntu 16.04.5 LTS

    - 4 GB RAM

2. Now, a new window will open in your default browser where you'll need to provide the FabDep Beta Key in order to complete the installation process.

3. Now you can start using FabDep right away.

For any technical queries related to FabDep, join our [Rocketchat channel](http://chat.fabdep.com/).